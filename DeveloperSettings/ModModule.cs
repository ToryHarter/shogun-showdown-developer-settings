﻿using HarmonyLib;
using System;
using System.Reflection;
using UnityModManagerNet;

namespace DeveloperSettings
{
    static class ModModule
    {
        public static UnityModManager.ModEntry? Mod;
        public static bool Enabled;
        public static Settings? SettingsConfig;

        static bool Load(UnityModManager.ModEntry modEntry)
        {
            try
            {
                Mod = modEntry;

                modEntry.OnToggle = OnToggle;
                modEntry.OnGUI = OnGUI;
                modEntry.OnSaveGUI = OnSaveGUI;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static bool OnToggle(UnityModManager.ModEntry modEntry, bool value)
        {
            var harmony = new Harmony("ShogunShowdownDeveloperSettings");

            if (value)
            {
                SettingsConfig = new Settings();
                SettingsConfig = Settings.Load<Settings>(modEntry);

                harmony.PatchAll(Assembly.GetExecutingAssembly());

                Mod!.Logger.Log("Mod Enabled.");
            }
            else
            {
                harmony.UnpatchAll("ShogunShowdownDeveloperSettings");

                Mod!.Logger.Log("Mod Disabled.");
            }

            Enabled = value;
            return true;
        }

        static void OnGUI(UnityModManager.ModEntry modEntry)
        {
            SettingsConfig.Draw(modEntry);
        }

        static void OnSaveGUI(UnityModManager.ModEntry modEntry)
        {
            SettingsConfig!.Save(modEntry);
        }
    }
}
