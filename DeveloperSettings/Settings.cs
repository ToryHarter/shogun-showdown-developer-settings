﻿using System;
using UnityModManagerNet;

namespace DeveloperSettings
{
    public class Settings : UnityModManager.ModSettings, IDrawable
    {
        [Draw(Label = "Skip Title Screen", Tooltip = "When the game launches skip the title screen and load directly in camp.")]
        public UINullableBool SkipTitleScreen = UINullableBool.Disabled;

        [Draw(Label = "Invulnerable", Tooltip = "Prevents taking damage from attacks.")]
        public UINullableBool Invulnerable = 0;

        [Draw(Label = "Always-On Info Mode ", Tooltip = "Allows Info-Mode to always be on when you hover over an enemy/tile/item/etc.")]
        public UINullableBool FullInfoMode = 0;

        public void OnChange() { }

        public override void Save(UnityModManager.ModEntry modEntry)
        {
            try
            {
                ModModule.Mod!.Logger.Log("Saving UnityModManager settings.");

                Save(this, modEntry);

                ModModule.Mod.Logger.Log("Successfully saved UnityModManager settings.");
            }
            catch (Exception e)
            {
                ModModule.Mod!.Logger.Error($"Error saving UnityModManager Settings: {e.InnerException}");
            }
        }

        public static bool? UINullableBoolToBool(UINullableBool value)
        {
            if (value == UINullableBool.Enabled) return true;
            if (value == UINullableBool.Disabled) return false;
            return null;
        }

        public static UINullableBool BoolToUINullableBool(bool? value)
        {
            if (!value.HasValue) return UINullableBool.Default;
            if (value.Value) return UINullableBool.Enabled;
            return UINullableBool.Disabled;
        }

        public enum UINullableBool
        {
            Default = 0,
            Enabled = 1,
            Disabled = 2
        }
    }
}
