﻿using HarmonyLib;
using System;

namespace DeveloperSettings.Patches
{
    static class DeveloperUtilsPatch
    {
        [HarmonyPatch(typeof(DeveloperUtils), "SkipTitleScreen", MethodType.Getter)]
        static class DeveloperUtils_SkipTitleScreen_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    if (ModModule.SettingsConfig!.SkipTitleScreen != Settings.UINullableBool.Default)
                    {
                        __result = Settings.UINullableBoolToBool(ModModule.SettingsConfig.SkipTitleScreen)!.Value;
                    }
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching Globals.{nameof(ModModule.SettingsConfig.SkipTitleScreen)}: {e.ToString()}");
                }
            }
        }

        [HarmonyPatch(typeof(DeveloperUtils), "Invulnerable", MethodType.Getter)]
        static class DeveloperUtils_Invulnerable_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    if (ModModule.SettingsConfig!.Invulnerable != Settings.UINullableBool.Default)
                    {
                        __result = Settings.UINullableBoolToBool(ModModule.SettingsConfig.Invulnerable)!.Value;
                    }
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching Globals.{nameof(ModModule.SettingsConfig.Invulnerable)}: {e.ToString()}");
                }
            }
        }

        [HarmonyPatch(typeof(DeveloperUtils), "CustomLocation", MethodType.Getter)]
        static class DeveloperUtils_CustomLocation_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    __result = false;
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching DeveloperUtils.CustomLocation: {e.ToString()}");
                }
            }
        }

        [HarmonyPatch(typeof(DeveloperUtils), "CustomHero", MethodType.Getter)]
        static class DeveloperUtils_CustomHero_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    __result = false;
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching DeveloperUtils.CustomHero: {e.ToString()}");
                }
            }
        }

        [HarmonyPatch(typeof(DeveloperUtils), "CustomLoadout", MethodType.Getter)]
        static class DeveloperUtils_CustomLoadout_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    __result = false;
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching DeveloperUtils.CustomLoadout: {e.ToString()}");
                }
            }
        }

        [HarmonyPatch(typeof(DeveloperUtils), "CustomDay", MethodType.Getter)]
        static class DeveloperUtils_CustomDay_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    __result = false;
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching DeveloperUtils.CustomDay: {e.ToString()}");
                }
            }
        }
    }
}
