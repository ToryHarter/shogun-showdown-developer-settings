﻿using HarmonyLib;
using System;

namespace DeveloperSettings.Patches
{
    static class GlobalsPatches
    {
        [HarmonyPatch(typeof(Globals), "Developer", MethodType.Getter)]
        static class Globals_Developer_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    __result = true;
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching Globals.Developer: {e.ToString()}");
                }
            }
        }

        [HarmonyPatch(typeof(Globals), "FullInfoMode", MethodType.Getter)]
        static class Globals_FullInfoMode_Patch
        {
            static void Postfix(ref bool __result)
            {
                try
                {
                    if (ModModule.SettingsConfig!.FullInfoMode != Settings.UINullableBool.Default)
                    {
                        __result = Settings.UINullableBoolToBool(ModModule.SettingsConfig.FullInfoMode)!.Value;
                    }
                }
                catch (Exception e)
                {
                    ModModule.Mod!.Logger.Error($"Error patching Globals.{nameof(ModModule.SettingsConfig.FullInfoMode)}: {e.ToString()}");
                }
            }
        }
    }
}
